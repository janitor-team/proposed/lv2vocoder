PLUGIN_NAME = vocoder

# These files go into the bundle
BUNDLE_FILES = manifest.ttl vocoder.ttl vocoder.so

CFLAGS = -c -Wall -fPIC -DPIC $(strip $(shell pkg-config --cflags lv2core))

GENDEP_SED_EXPR = "s/^\\(.*\\)\\.o *: /$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g"
GENDEP_C = set -e; $(GCC_PREFIX)gcc -MM $(CFLAGS) $< | sed $(GENDEP_SED_EXPR) > $@; [ -s $@ ] || rm -f $@

default: $(PLUGIN_NAME).lv2

%.o:%.c
	@echo "Compiling $< to $@ ..."
	@$(CC) $(CFLAGS) $< -o $@

%.d: %.c
	@echo "Generating dependency for $< to $@ ..."
	@$(GENDEP_C)

vocoder.so: vocoder.o
	@echo "Creating LV2 shared library $@ ..."
	@g++ -shared -fPIC $(LDFLAGS) $< -o $@

# The main target - the LV2 plugin bundle
$(PLUGIN_NAME).lv2: $(BUNDLE_FILES)
	@echo "Creating LV2 bundle $@ ..."
	@rm -rf $(PLUGIN_NAME).lv2
	@mkdir $(PLUGIN_NAME).lv2
	@cp $(BUNDLE_FILES) $(PLUGIN_NAME).lv2

clean:
	rm -rf *.o vocoder.so $(PLUGIN_NAME).lv2

install: $(PLUGIN_NAME).lv2
	@./install_lv2.sh $(PLUGIN_NAME).lv2
